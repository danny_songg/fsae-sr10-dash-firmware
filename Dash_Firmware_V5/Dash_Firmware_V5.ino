 /***************************************************************************************
  Author: Danny Song & Mitchell Sayer
  Program Name: Dash_Firmware_V5
  Brief: The main firmware of the SR10 Dashboard
  Date: Nov 22 2017
***************************************************************************************/

// Include all neccesary libraries
#include <Wire.h>   //14 Segment display wire library to drive I2C
#include <Adafruit_NeoPixel.h>  //Neo Pixel Library
#include <SPI.h>  //SPI used for comunication with CAN controller
#include <mcp_can.h>  //CanBus Library

//FIXME: Make tach sig pin 2(D2) , NEO PIXEL pin 3 (D3)
//Define all const.
#define Neopixel_PIN 3
#define GEAR_POS_PIN 2 
#define TACH_SIG_PIN 2
#define PIN_B 10
#define PIN_A 11
#define ENC_BUTTON_PIN 12
#define RED 90
#define GREEN 91
#define BLUE 92
#define SHIFT 99
#define LED_NUM 16
#define cs_pin 9

MCP_CAN CAN(cs_pin);                        //sets cs pin

//Define all Global Var.
byte brightness = 50; //for brightness control on display and tach
unsigned long current_time = 0;
unsigned long prev_time = 0;
long unsigned int enc_curtime = 0;
long unsigned int enc_prevtime = 0;
//unsigned long RPM; //probably dont need anymore 
unsigned int shiftRPM = 11100; //why longs tho
uint32_t curtime = 0;
uint32_t flashtime = 0;
bool Enc_A;
bool Enc_B;
bool Enc_A_Prev=0;
bool FLASH_ON = false;
unsigned int BRT = 16;
byte butt_counter = 0;
int Pos = 16000;
int Prev_Pos = 15999;

//Basic CanBus Vars
unsigned char len = 0;  
unsigned char buf[8]; 
unsigned int data_ID;  
unsigned int engine_speed;
unsigned int FL_wheel_speed;
unsigned int FR_wheel_speed;
signed int gear;
 
//All parameter setup
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_NUM, Neopixel_PIN, NEO_GRBW + NEO_KHZ800);
void Alphabet(char, unsigned int);
void neopixel(int, byte);
void tach();
int encoder(void);
void Bright();
void read_gear();


void setup() 
{
  //PINMODE SETUP
  pinMode(Neopixel_PIN, OUTPUT);
  pinMode(TACH_SIG_PIN, INPUT_PULLUP);
  pinMode(A2, INPUT);
  pinMode(PIN_B, INPUT_PULLUP);
  pinMode(PIN_A, INPUT_PULLUP);
  pinMode(ENC_BUTTON_PIN, INPUT_PULLUP);
  
  //NEOPIXEL intialization
  strip.begin();
  strip.show(); 
  
  //14 SEG LED DISP. Init.
  Wire.begin();
  Wire.beginTransmission(0x70);
  Wire.write(0b00100001);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(0b10000001);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(0b10100011);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(224+(16-1));
  Wire.endTransmission();
  for(unsigned int i=0; i<8; i++)
    {
      Wire.beginTransmission(0x70);
      Wire.write(i);
      Wire.write(0);
      Wire.endTransmission();
    }
  
  //Initialise interrupt for tach signal reporting
  Serial.begin(115200);
  attachInterrupt(digitalPinToInterrupt(2),tach_sig_interrupt, FALLING); 
  
  //Initialize CanBus @1mbps
  while (CAN_OK != CAN.begin(CAN_1000KBPS))              // init can bus : baudrate = 500k
  {
      Serial.println("CAN init failed, retry");
      delay(100);
  }
  Serial.println("CAN init ok");

  set_mask_filter();
  
  //Initiate global variables
  butt_counter = 0;
  }
  
  //TODO: complete the main loop sequence
void loop() 
{
  //Reading incoming CanBus messages
  if(CAN_MSGAVAIL == CAN.checkReceive())                  //chekcs for incoming data 
  {
    CAN.readMsgBuf(&len,buf);                             //read data 
    data_ID = CAN.getCanId();                             //gets ID of incoming data
    if (data_ID == 0x640)                                 //engine speed
    {
      Serial.print("Reading from ID ");
      Serial.println(data_ID, HEX);     //printing the ID in HEX

      engine_speed = buf[0]<<8;
      engine_speed = engine_speed+buf[1];
        
      Serial.print("Engine Speed = ");
      Serial.print(engine_speed);
      Serial.println("rpm");
      Serial.println("\n\n");
    }
    if(data_ID == 0x648)                                  //wheel speed
    {                                 
      Serial.print("Reading from ID ");
      Serial.println(data_ID, HEX);                         //printing the ID in HEX

      FL_wheel_speed = buf[0]<<8;
      FL_wheel_speed = FL_wheel_speed+buf[1];
      FR_wheel_speed = buf[2]<<8;
      FR_wheel_speed = FR_wheel_speed+buf[3];

      Serial.print("Front Left Wheel Speed = ");
      Serial.print(FL_wheel_speed * .1);
      Serial.println("km/h");
      Serial.print("Front Right Wheel Speed = ");
      Serial.print(FR_wheel_speed * .1);
      Serial.println("km/h");
      Serial.println("\n\n");
      
    }
    if(data_ID == 0x64D)                                  //gear pos
    {
      Serial.print("Reading from ID ");
      Serial.println(data_ID, HEX);     //printing the ID in HEX

      gear = buf[6] & 0x0F;

      Serial.print("Gear = ");
      Serial.println(gear);
      Serial.println("\n\n");
    }
  }
  /*unsigned long period = (current_time - prev_time);
  if(period != 0)
  {
    RPM = (60000/period);
    prev_time = current_time;
    noInterrupts();
    tach();
    interrupts();
  }
  */
  if(!(PINB&1<<4))
  {
    while(!(PINB&1<<4))
    {delay(10);}
    butt_counter++;
    if(butt_counter>=2)
    {
      butt_counter = 0;
    }
  }
  switch (butt_counter)
  {
    case 0:
      read_gear();
      tach();
      break;
    case 1:
      Bright();
      break;
    default:
      break;
  }
}


/**
****************************************************************************************
* @brief  Tach Signal Interrupt callback function
* @param[in]    void
* @return       void
****************************************************************************************
*/
void tach_sig_interrupt()
{
  current_time = millis();
}

/**
****************************************************************************************
* @brief  tachometer function
* @param[in]    void
* @return       void
****************************************************************************************
*/
void tach()
{
  if(engine_speed >= shiftRPM)
  {
    curtime = millis();
    if((curtime-flashtime)>50)
    {
      if(FLASH_ON)
      {
        neopixel(0,brightness);
        FLASH_ON = false;
      }
      else
      {
        neopixel(9,brightness);
        FLASH_ON = true;
      }
      flashtime = curtime;
    }  
  }
  else if((engine_speed >= 0)&&(engine_speed < shiftRPM))
  {
    if(engine_speed <4000){
      neopixel(0,brightness);
    }
    else if(engine_speed==4000)
    {
      neopixel(1,brightness);
    }
    else if(engine_speed==5000)
    {
      neopixel(2,brightness);
    }
    else if(engine_speed==6000)
    {
      neopixel(3,brightness);
    }
    else if(engine_speed==7000)
    {
      neopixel(4,brightness);
    }
    else if(engine_speed==8000)
    {
      neopixel(5,brightness);
    }
    else if(engine_speed==9000)
    {
      neopixel(6,brightness);
    }
    else if(engine_speed==10000)
    {
      neopixel(7,brightness);
    }
    else if(engine_speed==11000)
    {
      neopixel(8,brightness);
    }
  }
  else  
  {
    //Error signal - Input out of bounds
  }
}

/**
****************************************************************************************
* @brief  Brightnes
* @param[in]    void
* @return       void
****************************************************************************************
*/
void Bright()
{
  Pos+=(encoder());
  if(!(Pos==Prev_Pos))
  {
    BRT=1+(Pos%16);
    neopixel(8,BRT);
    delay(5);
    Wire.beginTransmission(0x70);
    Wire.write(224+(BRT-1));
    Wire.endTransmission();
    delay(5);
    Alphabet('b',0);
    Alphabet('r', 1);
    Alphabet('i', 2);
    Alphabet('t', 3);
    brightness = BRT;
  }
}

/**

/**
****************************************************************************************
* @brief  Gear POS
* @param[in]    void
* @return       void
****************************************************************************************
*/
void read_gear()  //TODO: complete GEAR POS function, need to figure out how to process data due to its noise
{
  Alphabet('G' , 0);
  Alphabet(0, 1);
  Alphabet(0, 3);
  //TODO: Write different cases for different gear
  //int gear = analogRead(GEAR_POS_PIN);
  switch(gear)
  {
    case 0:
          Alphabet('N', 2);
          break;
    case 1:
          Alphabet('1', 2);
          break;
    case 2:
          Alphabet('2', 2);
          break;
    case 3:
          Alphabet('3', 2);
          break;
    case 4:
          Alphabet('4', 2);
          break;
    case 5:
          Alphabet('5', 2);
          break;
    case 6:
          Alphabet('6', 2);
          break;                    
  }
  
}
/**
****************************************************************************************
* @brief  Sets CanBus Filter and Mask
* @param[in]    void
* @return       void
****************************************************************************************
*/
void set_mask_filter()
{
  CAN.init_Mask(0,0,0x7f2);                     //set both masks to 0x7f2
  CAN.init_Mask(1,0,0x7f2);

  CAN.init_Filt(0,0,0x640);                     //set all filters to 0x640
  CAN.init_Filt(1,0,0x640);
  CAN.init_Filt(2,0,0x640);
  CAN.init_Filt(3,0,0x640);
  CAN.init_Filt(4,0,0x640);
  CAN.init_Filt(5,0,0x640);
}
/**


****************************************************************************************
* @brief  Tach neopixel driver function
* @param[in]    color, brightness, amount(# LED)
* @return       void
****************************************************************************************
*/
void neopixel(int Input, byte bri)
{
  strip.setBrightness(bri);
  switch(Input)
  {
    case 0:
        strip.setPixelColor(0,0,0,0);
        strip.setPixelColor(15,0,0,0);
        strip.setPixelColor(1,0,0,0);
        strip.setPixelColor(14,0,0,0);
        strip.setPixelColor(2,0,0,0);
        strip.setPixelColor(13,0,0,0);
        strip.setPixelColor(3,0,0,0);
        strip.setPixelColor(12,0,0,0);
        strip.setPixelColor(4,0,0,0);
        strip.setPixelColor(11,0,0,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 1:
        strip.setPixelColor(0,0,0,(BRT*16)-1);
        strip.setPixelColor(15,0,0,(BRT*16)-1);
        strip.setPixelColor(1,0,0,0);
        strip.setPixelColor(14,0,0,0);
        strip.setPixelColor(2,0,0,0);
        strip.setPixelColor(13,0,0,0);
        strip.setPixelColor(3,0,0,0);
        strip.setPixelColor(12,0,0,0);
        strip.setPixelColor(4,0,0,0);
        strip.setPixelColor(11,0,0,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 2:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,(BRT*16)-1,0);
        strip.setPixelColor(14,0,(BRT*16)-1,0);
        strip.setPixelColor(2,0,0,0);
        strip.setPixelColor(13,0,0,0);
        strip.setPixelColor(3,0,0,0);
        strip.setPixelColor(12,0,0,0);
        strip.setPixelColor(4,0,0,0);
        strip.setPixelColor(11,0,0,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 3:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,(BRT*16)-1,0);
        strip.setPixelColor(14,0,(BRT*16)-1,0);
        strip.setPixelColor(2,0,(BRT*16)-1,0);
        strip.setPixelColor(13,0,(BRT*16)-1,0);
        strip.setPixelColor(3,0,0,0);
        strip.setPixelColor(12,0,0,0);
        strip.setPixelColor(4,0,0,0);
        strip.setPixelColor(11,0,0,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 4:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,(BRT*16)-1,0);
        strip.setPixelColor(14,0,(BRT*16)-1,0);
        strip.setPixelColor(2,0,(BRT*16)-1,0);
        strip.setPixelColor(13,0,(BRT*16)-1,0);
        strip.setPixelColor(3,0,(BRT*16)-1,0);
        strip.setPixelColor(12,0,(BRT*16)-1,0);
        strip.setPixelColor(4,0,0,0);
        strip.setPixelColor(11,0,0,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 5:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,(BRT*16)-1,0);
        strip.setPixelColor(14,0,(BRT*16)-1,0);
        strip.setPixelColor(2,0,(BRT*16)-1,0);
        strip.setPixelColor(13,0,(BRT*16)-1,0);
        strip.setPixelColor(3,0,(BRT*16)-1,0);
        strip.setPixelColor(12,0,(BRT*16)-1,0);
        strip.setPixelColor(4,0,(BRT*16)-1,0);
        strip.setPixelColor(11,0,(BRT*16)-1,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 6:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,(BRT*16)-1,0);
        strip.setPixelColor(14,0,(BRT*16)-1,0);
        strip.setPixelColor(2,0,(BRT*16)-1,0);
        strip.setPixelColor(13,0,(BRT*16)-1,0);
        strip.setPixelColor(3,0,(BRT*16)-1,0);
        strip.setPixelColor(12,0,(BRT*16)-1,0);
        strip.setPixelColor(4,0,(BRT*16)-1,0);
        strip.setPixelColor(11,0,(BRT*16)-1,0);
        strip.setPixelColor(5,0,(BRT*16)-1,0);
        strip.setPixelColor(10,0,(BRT*16)-1,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 7:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,((BRT*16)-1),0);
        strip.setPixelColor(14,0,((BRT*16)-1),0);
        strip.setPixelColor(2,0,((BRT*16)-1),0);
        strip.setPixelColor(13,0,((BRT*16)-1),0);
        strip.setPixelColor(3,0,((BRT*16)-1),0);
        strip.setPixelColor(12,0,((BRT*16)-1),0);
        strip.setPixelColor(4,0,((BRT*16)-1),0);
        strip.setPixelColor(11,0,((BRT*16)-1),0);
        strip.setPixelColor(5,0,(BRT*16)-1,0);
        strip.setPixelColor(10,0,(BRT*16)-1,0);
        strip.setPixelColor(6,0,(BRT*16)-1,0);
        strip.setPixelColor(9,0,(BRT*16)-1,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
    case 8:
        strip.setPixelColor(0,0,(BRT*16)-1,0);
        strip.setPixelColor(15,0,(BRT*16)-1,0);
        strip.setPixelColor(1,0,((BRT*16)-1),0);
        strip.setPixelColor(14,0,((BRT*16)-1),0);
        strip.setPixelColor(2,0,((BRT*16)-1),0);
        strip.setPixelColor(13,0,((BRT*16)-1),0);
        strip.setPixelColor(3,0,((BRT*16)-1),0);
        strip.setPixelColor(12,0,((BRT*16)-1),0);
        strip.setPixelColor(4,0,((BRT*16)-1),0);
        strip.setPixelColor(11,0,((BRT*16)-1),0);
        strip.setPixelColor(5,0,(BRT*16)-1,0);
        strip.setPixelColor(10,0,(BRT*16)-1,0);
        strip.setPixelColor(6,0,(BRT*16)-1,0);
        strip.setPixelColor(9,0,(BRT*16)-1,0);
        strip.setPixelColor(7,0,(BRT*16)-1,0);
        strip.setPixelColor(8,0,(BRT*16)-1,0);
        break;
    case 9:
        strip.setPixelColor(0,(BRT*16)-1,0,0);
        strip.setPixelColor(15,(BRT*16)-1,0,0);
        strip.setPixelColor(1,(BRT*16)-1,0,0);
        strip.setPixelColor(14,(BRT*16)-1,0,0);
        strip.setPixelColor(2,(BRT*16)-1,0,0);
        strip.setPixelColor(13,(BRT*16)-1,0,0);
        strip.setPixelColor(3,(BRT*16)-1,0,0);
        strip.setPixelColor(12,(BRT*16)-1,0,0);
        strip.setPixelColor(4,(BRT*16)-1,0,0);
        strip.setPixelColor(11,(BRT*16)-1,0,0);
        strip.setPixelColor(5,(BRT*16)-1,0,0);
        strip.setPixelColor(10,(BRT*16)-1,0,0);
        strip.setPixelColor(6,(BRT*16)-1,0,0);
        strip.setPixelColor(9,(BRT*16)-1,0,0);
        strip.setPixelColor(7,(BRT*16)-1,0,0);
        strip.setPixelColor(8,(BRT*16)-1,0,0);
        break;
  default:
        strip.setPixelColor(0,0,0,0);
        strip.setPixelColor(15,0,0,0);
        strip.setPixelColor(1,0,0,0);
        strip.setPixelColor(14,0,0,0);
        strip.setPixelColor(2,0,0,0);
        strip.setPixelColor(13,0,0,0);
        strip.setPixelColor(3,0,0,0);
        strip.setPixelColor(12,0,0,0);
        strip.setPixelColor(4,0,0,0);
        strip.setPixelColor(11,0,0,0);
        strip.setPixelColor(5,0,0,0);
        strip.setPixelColor(10,0,0,0);
        strip.setPixelColor(6,0,0,0);
        strip.setPixelColor(9,0,0,0);
        strip.setPixelColor(7,0,0,0);
        strip.setPixelColor(8,0,0,0);
        break;
  }
  strip.show();
}

/**
****************************************************************************************
* @brief  eocnder driver function
* @param[in]    void
* @return       void
****************************************************************************************
*/
int encoder()
{ 
  int pos = 0;
  enc_curtime=millis();
  if(enc_curtime>=(enc_prevtime+5))
  {
    Enc_A=(PINB&(1<<3));
    Enc_B=(PINB&(1<<2));
    if((!Enc_A)&(Enc_A_Prev))
    {
      if(Enc_B)
      {
        pos++;
      }
      else
      {
        pos--;
      }
    }
    Enc_A_Prev = Enc_A;
    enc_prevtime=enc_curtime;
  }
  return pos;
}
/**
****************************************************************************************
* @brief  14 Segments Display driver function
* @param[in]    Input, dig
* @return       void
****************************************************************************************
*/
void Alphabet(char Input, unsigned int dig)
{
unsigned int Digit=dig*2;
switch(Input)
  {
case 'A':
case 'a':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xF7);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'B':
case 'b':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xCF);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x12);
  Wire.endTransmission();
  break;
case 'C':
case 'c':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x39);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'D':
case 'd':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x0F);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x12);
  Wire.endTransmission();
  break;
case 'E':
case 'e':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xF9);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'F':
case 'f':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xF1);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'G':
case 'g':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xBD);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'H':
case 'h':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xF6);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'I':
case 'i':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x09);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x12);
  Wire.endTransmission();
  break;
case 'J':
case 'j':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x1E);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'K':
case 'k':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x70);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x24);
  Wire.endTransmission();
  break;
case 'L':
case 'l':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x38);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'M':
case 'm':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x36);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x05);
  Wire.endTransmission();
  break;
case 'N':
case 'n':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x36);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x21);
  Wire.endTransmission();
  break;
case 'O':
case 'o':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x3F);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'P':
case 'p':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xC3);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x12);
  Wire.endTransmission();
  break;
case 'Q':
case 'q':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x3F);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x60);
  Wire.endTransmission();
  break;
case 'R':
case 'r':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xF3);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x20);
  Wire.endTransmission();
  break;
case 'S':
case 's':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x8D);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x01);
  Wire.endTransmission();
  break;
case 'T':
case 't':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x01);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x12);
  Wire.endTransmission();
  break;
case 'U':
case 'u':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x3E);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case 'V':
case 'v':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x30);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x0C);
  Wire.endTransmission();
  break;
case 'W':
case 'w':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x36);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x28);
  Wire.endTransmission();
  break;
case 'X':
case 'x':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x2D);
  Wire.endTransmission();
  break;
case 'Y':
case 'y':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x15);
  Wire.endTransmission();
  break;
case 'Z':
case 'z':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x09);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x0C);
  Wire.endTransmission();
  break;
case '1':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x12);
  Wire.endTransmission();
  break;
case '2':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xDB);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '3':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xCF);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '4':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xE6);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '5':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xED);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '6':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xFD);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '7':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x01);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x14);
  Wire.endTransmission();
  break;
case '8':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xFF);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '9':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xEF);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
case '0':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x3F);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x0C);
  Wire.endTransmission();
  break;
case '-':
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0xC0);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
default:
  Wire.beginTransmission(0x70);
  Wire.write(Digit);
  Wire.write(0x00);
  Wire.endTransmission();
  Wire.beginTransmission(0x70);
  Wire.write(Digit+1);
  Wire.write(0x00);
  Wire.endTransmission();
  break;
  }
}


